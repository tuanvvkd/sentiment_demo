'use strict';

module.exports.detectEntity 			= detectEntity;
module.exports.detectEntityPosts 	= detectEntityPosts;
let key  		= 'technology_and_computing';
let API_KEY  	= "ebbfc3868dbe7bf6268d86233852d2e9";
let endpoint  	= "entities";

var Api 			= require('rosette-api');
const request 	= require("request");
const fetch  	= require('node-fetch');

function detectEntity(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		const params = req.query;
		const fid = params.post_id ? params.post_id : null;

		let dbQuery = {
			query : {
				"$select" : {
					_id : 0,
					fid : 1,
					message : 1
				}
			}
		}

		if (fid != null ) {
			dbQuery.query.fid = fid;
		}

		postService.find(dbQuery).then(result => {
			if (result.total > 0) {
				let post = result.data[0];
				checkAndUpdateEnity(postService, post.fid, post.message).then(result => {
					res.json({status : "ok"});
				}).catch(error => {
					res.json({status : "failed"});
				})
			} else {
				res.json('data is empty, please feed me more data');
			}
		}).catch(error => {
			console.log("Error when trying to get all groups from database. Message :", error);
			res.json({'error' : error});
		})
	};
};

function detectEntityPosts(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		const params = req.query;
		const limit = params.limit ? params.limit: 100;

		getPostIdsFromDb(limit, processListPost).then(result => {
			res.json("done");
		}).catch(error => {
			res.json("error");
		})

		function getPostIdsFromDb(limit, callback) {
			return new Promise((resolve, result) => {
				let dbQuery = {
					query : {
						$limit : limit,
						"$select" : {
							_id : 0,
							fid : 1,
							message : 1
						},
						$or : [
							{ entities : { $exists : false } },
							{ entities : null }
						]
					}
				}

				postService.find(dbQuery).then(result => {
					if (result.total > 0) {
						let listPost = result.data;
						callback(listPost).then(result => {
							resolve(getPostIdsFromDb(limit, callback));
						}).catch(error => {
							resolve(getPostIdsFromDb(limit, callback));
						})
					} else {
						console.log('data is empty, please feed me more data');
						resolve(true);
					}
				}).catch(error => {
					console.log("Error when trying to get all groups from database. Message :", error);
					reject(false);
				})
			})
		}

		function processListPost(listPost) {
			return new Promise((resolve, result) => {
				let waiter = []; 
				if (listPost.length > 0) {
					listPost.forEach(post => {
						waiter.push(checkAndUpdateEnity(postService, post.fid, post.message));
					});

					Promise.all(waiter).then(result => {
						console.log("Update entities for " + listPost.length + " post successfully!");
						resolve(true);
					}).catch(error => {
						console.log("Error when trying to detect entities of posts. Error " + error);
						reject(false);
					})
				} else {
					console.log("List post is empty. DONE!");
					resolve(true);
				}
			})
		}
	};
}

function checkAndUpdateEnity(postService, post_id, message) {
	return new Promise((resolve, reject) => {
		var api = new Api(API_KEY);
		api.parameters.content = message;
		api.parameters.language = "vie";
		 
		api.rosette(endpoint, function(err, res){
			let result = []
		    if(err){
		        console.log(err);
		        reject(false);
		    } else {
		    	let result = JSON.stringify(res, null, 2);
		    	let jsonResult = JSON.parse(result);
		        result = getEntityFromBody(jsonResult);
		        updateEntityPost(postService, post_id, result).then(result => {
		        	resolve(true);
		        }).catch(error => {
		        	console.log("Error when updating post id " + post_id + " with entities. Error: " + error);
		        	reject(false);
		        });
		    }
		})
	})
}

function getEntityFromBody(jsonObject) {
	let entities = jsonObject.entities;
	let result = [];
	entities.forEach(entity => {
		let temp = {
			type : entity.type,
			mention : entity.mention,
			normalized : entity.normalized,
			count : entity.count
		}
		result.push(entity);
	});
	
	return result;
}

function updateEntityPost(postService, fid, entities) {
	return new Promise((resolve, reject) => {
		postService.find({ 
			query : {
				fid : fid 
			} 
		}).then (result => {
			if (result.total > 0) {
				postService.update(result.data[0]._id, {$set : { entities : entities }}).then(status => {
					console.log("Update entities of post_fid " + fid + " with entities " + entities + " successfully!");
					resolve(true);
				}).catch(error => {
				    console.log("Cannot update entities of post_fid " + fid + ". Error message", error);
				    reject(false);
				});
			} else {
				console.log("Post fid " + fid + " doesn't exists. Error message", error);
				reject(false);
			}
		}).catch(error => {
			console.log("Cannot find post_fid " + fid + ". Error message", error);
		    reject(false);
		})
	})
}