'use strict';

module.exports.getAllPosts = getAllPosts;
module.exports.updatePost = updatePost;
module.exports.getSamsungPosts = getSamsungPosts;

function getAllPosts(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		let { length, start, draw, post_type } = req.body;
		(post_type !== undefined) ? post_type : post_type = 'adasia';

		let dbQuery = {
			query : {
				"$limit" : length,
				"$skip" : start,
				"$select" : {
					_id : 0,
					fid : 1,
					from_user: 1,
					message : 1,
					created_date : 1, 
					sentiment: 1,
					entities : 1
				},
				post_type : post_type
			}
		}

		let finalResult = { 
			data : [],
			draw : draw,
			recordsFiltered : 0,
			recordsTotal : 0
		}

		postService.find(dbQuery).then(posts => {
			finalResult.recordsTotal = posts.total;
			finalResult.recordsFiltered = posts.total;
			let listPost = posts.data;

			for (var i = 0; i <  listPost.length; i++) {
				let crrPost = listPost[i];
				let actionHTML = '<a onclick="open_form_sentiment(this, [id])" class="btn btn-sm blue"><i class="fa fa-meh-o"></i> Sentiment</a><br>'+
					'<a onclick="open_form_entity(this, [id])" class="btn btn-sm green"><i class="fa fa-meh-o"></i> Entity</a><br>'+
        			'<a onclick="open_form_edit(this, [id])" class="btn btn-sm btn-outline grey-salsa"><i class="fa fa-pencil"></i> Edit</a><br>'+
        			'<a onclick="open_confirm_delete(this, [id])" class="btn btn-sm btn-outline red"><i class="fa fa-times"></i> Delete</a><br>';
				let currentEntitesString = '';
				if (crrPost.entities != null) {
					crrPost.entities.forEach(entity => {
						currentEntitesString += entity.normalized + ", ";
					})
				}

				currentEntitesString = currentEntitesString.trim();
				currentEntitesString = currentEntitesString.substr(0 , currentEntitesString.length - 1);

				let temp = [i+1, crrPost.fid, crrPost.from_user, crrPost.message, crrPost.created_date, crrPost.sentiment, currentEntitesString, actionHTML];
				finalResult.data.push(temp);
			}
			res.json(finalResult);
		}).catch(error => {
			console.log("Error when trying to get all groups from database. Message :", error);
			res.json(finalResult);
		})
	};
};

function getSamsungPosts(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		let { length, start, draw } = req.body;
		let post_type = 'samsung';

		let dbQuery = {
			query : {
				"$limit" : length,
				"$skip" : start,
				post_type : post_type,
				"$select" : {
					_id : 0,
					fid : 1,
					from_user: 1,
					message : 1,
					created_date : 1, 
					sentiment: 1,
					entities : 1
				}
			}
		}

		let finalResult = { 
			data : [],
			draw : draw,
			recordsFiltered : 0,
			recordsTotal : 0
		}

		postService.find(dbQuery).then(posts => {
			finalResult.recordsTotal = posts.total;
			finalResult.recordsFiltered = posts.total;
			let listPost = posts.data;

			for (var i = 0; i <  listPost.length; i++) {
				let crrPost = listPost[i];
				let temp = [i+1, crrPost.fid, crrPost.from_user, crrPost.message, crrPost.created_date, crrPost.sentiment];
				finalResult.data.push(temp);
			}
			res.json(finalResult);
		}).catch(error => {
			console.log("Error when trying to get all groups from database. Message :", error);
			res.json(finalResult);
		})
	};
}

function updatePost(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		let { post_id, content, post_type} = req.body;
		console.log(post_id, content, post_type);
		(post_type !== undefined) ? post_type : post_type = 'adasia';
		let dbQuery = {
			query : {
				fid : post_id,
				post_type : post_type
			}
		}

		let finalResult = { 'status' : 'ok'};
		postService.find(dbQuery).then(result => {
			if (result.total > 0) {
				postService.update(result.data[0]._id, {
					'$set': { 
						message : content,
						sentiment : ''
					}
				}).then(result => {
					res.json(finalResult);
				}).catch(error => {
					finalResult.status = 'failed';
					res.json(finalResult);
				});
			} else {
				finalResult.status = 'empty';
				res.json(finalResult);
			}
		}).catch(error => {
			finalResult.status = 'error';
			res.json(finalResult);
		})
	};
};