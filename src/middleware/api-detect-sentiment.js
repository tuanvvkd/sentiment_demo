'use strict';

module.exports.detectSentiment 		= detectSentiment;
module.exports.detectSentimentPosts 	= detectSentimentPosts;
let apiKey = "59b67e779ca922d63458d87e4bdb9c825ad18b60";
let endpoint = "https://api.repustate.com/v3/";
let api_sentiment_single = "/score.json";
let api_sentiment_multi = "/bulk-score.json";
var request = require("request");

const fetch = require('node-fetch');

let sentiment_type = {"positive" : 1, "negative" : -1 , "neutral" : 0, "unknown" : null };

function detectSentiment(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		const params = req.query;
		const offset = params.offset ? params.offset: 0;
		const limit = params.limit ? params.limit: 1;
		const fid = params.post_id ? params.post_id : null;

		let dbQuery = {
			query : {
				$limit : limit,
				$skip : offset,
				"$select" : {
					_id : 0,
					fid : 1,
					message : 1
				}
			}
		}

		if (fid != null ) {
			dbQuery.query.fid = fid;
		}

		postService.find(dbQuery).then(result => {
			if (result.total > 0) {
				let post = result.data[0];
				checkAndUpdateSentiment(postService, post.fid, post.message).then(result => {
					res.json({status : "ok"});
				}).catch(error => {
					res.json({status : "failed"});
				})
			} else {
				res.json('data is empty, please feed me more data');
			}
		}).catch(error => {
			console.log("Error when trying to get all groups from database. Message :", error);
			res.json({'error' : error});
		})
	};
};

function getSentimentFromScore(score){
	if (score == 0) {
		return "neutral";
	}
	if (score <= 1 && score > 0) {
		return "positive";
	}
	if (score < 0 && score >= -1) {
		return "negative";
	}
	return "unknown";
}

function detectSentimentPosts(app) {
	const postService = app.service('posts');
	return function(req, res, next) {
		const params = req.query;
		const limit = params.limit ? params.limit: 100;

		getPostIdsFromDb(limit, processListPost).then(result => {
			res.json("done");
		}).catch(error => {
			res.json("error");
		})

		function getPostIdsFromDb(limit, callback) {
			return new Promise((resolve, result) => {
				let dbQuery = {
					query : {
						$limit : limit,
						"$select" : {
							_id : 0,
							fid : 1,
							message : 1
						},
						$or : [
							{ sentiment : { $exists : false } },
							{ sentiment : null }
						]
					}
				}

				postService.find(dbQuery).then(result => {
					if (result.total > 0) {
						let listPost = result.data;
						callback(listPost).then(result => {
							resolve(getPostIdsFromDb(limit, callback));
						}).catch(error => {
							resolve(getPostIdsFromDb(limit, callback));
						})
					} else {
						console.log('data is empty, please feed me more data');
						resolve(true);
					}
				}).catch(error => {
					console.log("Error when trying to get all groups from database. Message :", error);
					reject(false);
				})
			})
		}

		function processListPost(listPost) {
			return new Promise((resolve, result) => {
				let waiter = []; 
				if (listPost.length > 0) {
					listPost.forEach(post => {
						waiter.push(checkAndUpdateSentiment(postService, post.fid, post.message));
					});

					Promise.all(waiter).then(result => {
						console.log("Update sentiment for " + listPost.length + " post successfully!");
						resolve(true);
					}).catch(error => {
						console.log("Error when trying to detect sentiment of posts. Error " + error);
						reject(false);
					})
				} else {
					console.log("List post is empty. DONE!");
					resolve(true);
				}
			})
		}
	};
}

function checkAndUpdateSentiment(postService, post_id, message) {
	return new Promise((resolve, reject) => {
		let data_to_sent = { 
			lang: 'vi', 
			text : message
		}

		var options = {
			method: 'POST',
			url: endpoint + apiKey + api_sentiment_single,
			qs: data_to_sent,
			headers: { 'content-type': 'application/x-www-form-urlencoded' },
			form: {} 
		};

		request(options, function (error, response, body) {
			if (error) {
				console.log("error when trying to find sentiment of post ID " + post_id + ". Error " + error);
				reject(false);
			}
			console.log("Sentiment result of post id " + post_id + ": " + body);
			let sentiment = "unknown";
			try {
				let jsonBody = JSON.parse(body);
				sentiment = getSentimentFromScore(jsonBody.score);
			} catch (error) {
				console.log("Unknown detected, post id " + post_id);
			}
			updateSentimentPost(postService, post_id, sentiment).then(result => {
				resolve(true);
			}).catch(error => {
				reject(false);
			})
		});
	})
}

function updateSentimentPost(postService, fid, sentiment) {
	return new Promise((resolve, reject) => {
		postService.find({ 
			query : {
				fid : fid 
			} 
		}).then (result => {
			if (result.total > 0) {
				postService.update(result.data[0]._id, {$set : { sentiment : sentiment }}).then(status => {
					console.log("Update sentiment of post_fid " + fid + " with sentiment " + sentiment + " successfully!");
					resolve(true);
				}).catch(error => {
				    console.log("Cannot update sentiment of post_fid " + fid + ". Error message", error);
				    reject(false);
				});
			} else {
				console.log("Post fid " + fid + " doesn't exists. Error message", error);
				reject(false);
			}
		}).catch(error => {
			console.log("Cannot find post_fid " + fid + ". Error message", error);
		    reject(false);
		})
	})
}