'use strict';

function getAllGroups(app) {
	const groupService = app.service('groups');
	return function(req, res, next) {
		let dbQuery = {
			query : {
				"$select" : {
					_id : 0,
					group_key : 1,
					group_name : 1
				}
			}
		}

		groupService.find(dbQuery).then(groups => {
			res.json(groups.data);
		}).catch(error => {
			console.log("Error when trying to get all groups from database. Message :", error);
			res.json([]);
		})
	};
};

function getGroupInfo(app) {
	return function(req, res, next) {
		console.log("getGroupInfo");
		const groupService = app.service('groups');
		let params = req.query;
		let group_key = params.group_key || null;

		let result = [];
		let dbQuery = {
			query : {
				group_key : group_key,
				"$select" : {
					_id : 0
				},
				"$limit" : 1
			}
		}

		groupService.find(dbQuery).then(result => {
			res.json(result.data[0]);
		}).catch(error => {
			console.log("Error when trying to get all groups from database. Message :", error);
			res.json(result);
		})
	};
}

module.exports.getAllGroups = getAllGroups;
module.exports.getGroupInfo = getGroupInfo;