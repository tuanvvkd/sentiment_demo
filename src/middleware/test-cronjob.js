'use strict';

module.exports = function(app) {
  return function(req, res, next) {
  	let now = new Date();
    console.log("Call cronjob at " + now.toISOString());
    res.json({call_at: now.toISOString()});
  };
};
