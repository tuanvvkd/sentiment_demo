'use strict';
const handler     = require('feathers-errors/handler');
const signup      = require('./signup');
const notFound    = require('./not-found-handler');
const sentiment   = require('./api-detect-sentiment');
const entity      = require('./api-detect-entity');
const post        = require('./api-get-post');
const logger      = require('./logger');

module.exports = function() {
  const app = this;

  app.post('/signup', signup(app));
  app.get('/api/detectsentiment', sentiment.detectSentiment(app));
  app.get('/api/detectsentimentposts', sentiment.detectSentimentPosts(app));
  app.get('/api/detectentity', entity.detectEntity(app));
  app.get('/api/detectentityposts', entity.detectEntityPosts(app));
  app.post('/api/getposts', post.getAllPosts(app));
  app.post('/api/getsamsungposts', post.getSamsungPosts(app));
  app.post('/api/updatepost', post.updatePost(app));
  
  app.use(notFound());
  app.use(logger(app));
  app.use(handler());
};