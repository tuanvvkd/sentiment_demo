'use strict';

module.exports.getInsightPosts = getInsightPosts;
module.exports.getInsightKols = getInsightKols;
module.exports.getInsightAdvocatorKols = getInsightAdvocatorKols;

var MongoClient 		= require('mongodb').MongoClient,
	assert 			= require('assert'),
	https 			= require('https'),
	util 			= require('util'),
	fetch 			= require('node-fetch');

// Connection URL
var mongoConnectionString = "mongodb://adasiaAdmin:m5v7cQUatBdXaFgK@125.212.241.144:27017/adasia";
var analysisDbPostLink = "http://adasia-dashboard.kyanon.digital/import-dashboard-facebook-post.php";
var analysisDbKolLink = "http://adasia-dashboard.kyanon.digital/import-dashboard-report-kol-data.php"
var analysisDbAvocatorLink = "http://adasia-dashboard.kyanon.digital/import-dashboard-avocator.php";

function getInsightPosts(app) {
  	return function(req, res, next) {
	  	let params = req.query;
	  	let group_key = params.group_key ? params.group_key : "kol";
	  	let date_from = params.date_from ? params.date_from : '2016-01-01';
	  	let date_to = params.date_to ? params.date_to : '2018-01-01';

	  	var collection_name = "posts_insights_" + group_key + "_" + date_from + "_" + date_to;

		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);	
			console.log("Connected correctly to server");

			let waiter = [];

			waiter.push(new Promise((resolve_lv1, reject_lv1) => {
				db.collection(collection_name).find().toArray(function(err, docs) {
					assert.equal(null, err);
					if (docs.length > 0) {
						resolve_lv1(docs);
						db.close();
					} else {
						return calculateInsightsPostsOfGroupUser(group_key, date_from, date_to).then(result => {
							return db.collection(collection_name).find().toArray(function(err, docs) {
								assert.equal(null, err);
								resolve_lv1(docs);
								db.close();
							})
						}).catch(error => {
							console.log("Error when performing map reduce: ", error);
						})
					}
				})
			}));
				
			Promise.all(waiter).then(result => {
				// only one element so hardcode to 0;
				createJsonPostData(result[0], group_key).then(dataTobeSent => {
					sendData(analysisDbPostLink, dataTobeSent, null).then(status => {
						console.log("========== status:" , status);
						if (status == true) {
							res.json({status : 'ok'});
						} else {
							res.json({status : 'failed'});
						}
					}).catch(error => {
						console.log("Error :", error);
					})
				}).catch(error => {
					console.log("Error :", error);
				})
			}).catch(error => {
				console.log("Error when sending value to analysis database: ", error);
			})
		})
	}
}

function getInsightKols(app) {
	return function(req, res, next) {
	  	let params = req.query;
	  	let group_key = params.group_key ? params.group_key : "kol";
	  	let date_from = params.date_from ? params.date_from : '2016-01-01';
	  	let date_to = params.date_to ? params.date_to : '2018-01-01';

	  	var collection_name = "kols_insights_" + group_key + "_" + date_from + "_" + date_to;

		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);	
			console.log("Connected correctly to server");

			let waiter = [];

			waiter.push(new Promise((resolve_lv1, reject_lv1) => {
				db.collection(collection_name).find().toArray(function(err, docs) {
					assert.equal(null, err);
					if (docs.length > 0) {
						resolve_lv1(docs);
						db.close();
					} else {
						calculateInsightsKolsOfGroupUser(group_key, date_from, date_to).then(result => {
							db.collection(collection_name).find().toArray(function(err, docs) {
								assert.equal(null, err);
								resolve_lv1(docs);
								db.close();
							})
						}).catch(error => {
							console.log("Error when performing map reduce: ", error);
						})
					}
				})
			}));
				
			Promise.all(waiter).then(result => {
				// only one element so hardcode to 0;
				createJsonKolData(result[0], group_key).then(dataTobeSent => {
					sendData(analysisDbKolLink, dataTobeSent, null).then(status => {
						if (status == true) {
							res.json({status : 'ok'});
						} else {
							res.json({status : 'failed'});
						}
					}).catch(error => {
						console.log("Error :", error);
					})
				}).catch(error => {
						console.log("Error :", error);
				})
			}).catch(error => {
				console.log("Error when sending value to analysis database: ", error);
			})
		})
	}
}

function getInsightAdvocatorKols(app) {
	return function(req, res, next) {
	  	let params = req.query;
	  	let group_key = params.group_key ? params.group_key : "kol";
	  	let date_from = params.date_from ? params.date_from : '2016-01-01';
	  	let date_to = params.date_to ? params.date_to : '2018-01-01';
	  	let limit = params.limit ? params.limit : 20;

	  	var collection_name = "kols_insights_avocators_" + group_key + "_" + date_from + "_" + date_to;

		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);	
			console.log("Connected correctly to server");

			let waiter = [];

			waiter.push(new Promise((resolve_lv1, reject_lv1) => {
				db.collection(collection_name).find().toArray(function(err, docs) {
					assert.equal(null, err);
					if (docs.length > 0) {
						getTopAvocatorOfGroupUser(group_key, collection_name, limit).then(advocators => {
							resolve_lv1(advocators);
						});
						db.close();
					} else {
						calculateInsightsAdvocatorsOfGroupUser(group_key, date_from, date_to).then(status => {
							getTopAvocatorOfGroupUser(group_key, collection_name, limit).then(advocators => {
								resolve_lv1(advocators);
							});
							db.close();
						}).catch(error => {
							console.log("Error when performing map reduce: ", error);
						})
					}
				})
			}));
				
			Promise.all(waiter).then(advocators => {
				// only one element so hardcode to 0;
				createJsonKolAdvocatorData(advocators[0], group_key).then(dataTobeSent => {
					// res.json(dataTobeSent);
					sendData(analysisDbAvocatorLink, dataTobeSent, null).then(status => {
						if (status == true) {
							res.json({status : 'ok'});
						} else {
							res.json({status : 'failed'});
						}
					}).catch(error => {
						console.log("Error :", error);
					})
				}).catch(error => {
					console.log("Error :", error);
				})
			}).catch(error => {
				console.log("Error when sending value to analysis database: ", error);
			})
		})
	}
}

function getTopAvocatorOfGroupUser(group_key, collection_name, limit) {
	return new Promise((resolve, reject) => {
		findUserIdOfGroupUser(group_key).then(user_ids => {
			getPostIdOfUserIds(user_ids).then(post_ids => {
				let getTopAvocatorPromises = [];
				post_ids.forEach(post_id => {
					getTopAvocatorPromises.push(getTopAdvocatorOfPostId(collection_name, post_id, limit))
				})
				let result = [];
				Promise.all(getTopAvocatorPromises).then(results => {
					results.forEach(data => {
						data.forEach(post => {
							result.push(post);
						})
					})
					resolve(result);
				})
			})
		})
	})
}

function getTopAdvocatorOfPostId(collection_name, post_id, limit) {
	return new Promise((resolve, reject) => {
		MongoClient.connect(mongoConnectionString, function(err, db) {
			db.collection(collection_name).find({"_id.post_id" : post_id }).sort({"value.weight" : -1}).limit(limit).toArray(function(err, foundAdvocators) {
				if (err != null) console.log(err);
				console.log("Get list of " + foundAdvocators.length + " advocator of post ID " + post_id + " successfully!");
				db.close();
				resolve(foundAdvocators);
			})
		});
	})
}

function sendData(url, data, options) {
	return new Promise((resolve, reject) =>{
		if (options) {
			// TODO
			// Xử lí cho options
		}
		// TODO xử lí cho các trường hợp dữ liệu quá lớn
		let dataLength = data.response.resultData.length;
		console.log("data size: ", dataLength);
		if (dataLength > 100) {
			
		}
		// POST by Node Fetch
		return fetch(url, {  
			method: 'POST',
			headers: {  
				"Content-type": "application/json; charset=UTF-8",
				"Data-type": "JSON"
			},
			body: JSON.stringify(data)
		}).then(function (res) {
			console.log('Request succeeded with JSON response');
			resolve(true);
			// For Debugging
			// console.log(res.ok);
			// console.log(res.status);
			// console.log(res.statusText);
			// console.log(res.headers.raw());
			// console.log(res.headers.get('content-type'));
		}).catch(function (error) {  
			console.log('Request failed', error);  
			resolve(false);
		});
	})
}

function createJsonPostData(data, group_key) {
	return new Promise((resolve, reject) =>{
		// console.log("Data to be convert to JSON:", data);
		var result = {
			response : {
				flagStatusData : true,
				resultData : []
			}
		}

		data.forEach(insight => {
			let info = {
				facebook_id				: insight.value.user_id,	
				post_id					: insight.value.post_id,	
				post_content			: insight.value.post_content,
				published_date			: Date.parse(insight.value.published_date) / 1000,
				crawled_date				: Date.parse(insight.value.crawled_date) / 1000,
				af_point					: insight.value.af_point,
				af_earning_value			: insight.value.earning_value,
				af_paid_value			: insight.value.paid_value,
				quantity_of_like			: insight.value.total_paid_like,
				like_paid_value			: insight.value.total_paid_like,
				like_earning_value		: insight.value.total_earn_like,
				quantity_of_comment		: insight.value.total_paid_comment,
				comment_paid_value		: insight.value.total_paid_comment,
				comment_earning_value	: insight.value.total_earn_comment,
				quantity_of_share		: insight.value.total_paid_share,
				share_paid_value			: insight.value.total_paid_share,
				share_earning_value		: insight.value.total_earn_share,
				post_type				: insight.value.post_type
			}
			result.response.resultData.push(info);
		})
		resolve(result);
	})
}

function createJsonKolData(data, group_key) {
	return new Promise((resolve, reject) =>{
		// console.log("Data to be convert to JSON:", data);
		var result = {
			response : {
				flagStatusData : true,
				resultData : []
			}
		}

		data.forEach(insight => {
			let info = {
				facebook_id: 				insight._id.fid,
				kol_name: 				 	null,
				kol_af_point: 				insight.value.kol_af_point,
				kol_like_point: 				insight.value.kol_like_point,
				kol_comment_point: 			insight.value.kol_comment_point,
				kol_share_point: 			insight.value.kol_share_point,
				kol_af_paid_value: 			insight.value.kol_af_paid_value,
				kol_af_earning_value: 		insight.value.kol_af_earning_value,
				kol_comment_paid_value: 		insight.value.kol_comment_paid_value,
				kol_comment_earning_value: 	insight.value.kol_comment_earning_value,
				kol_like_paid_value: 		insight.value.kol_like_paid_value,
				kol_like_earning_value: 		insight.value.kol_like_earning_value,
				kol_share_paid_value: 		insight.value.kol_share_paid_value,
				kol_share_earning_value: 	insight.value.kol_share_earning_value,
				brand_mention: 				"",
				share_of_voice: 				"",
				interest: 				 	"",
				response_rate: 				"",
				country: 				 	"",
				category: 				 	"",
				followers: 				 	0,
				gender: 				 	"",
				age: 				 		"",
				industry_mentioned: 			"",
				created_date: 				"",
				lastest_date: 				"",
				client: 				 		""
			}
			result.response.resultData.push(info);
		})

		findUserIdOfGroupUser(group_key).then(foundIds => {
			findUserNames(foundIds).then(foundUserInfo => {
				result.response.resultData.forEach(kolData => {
					let kol_id = kolData.facebook_id;
					foundUserInfo.forEach(foundInfo => {
						if (foundInfo.fid === kol_id) {
							kolData.kol_name = foundInfo.name,
							kolData.followers = foundInfo.followers
						}
					})
				});
				console.log("Updated KOL information successfully!");
				resolve(result);		
			}).catch(error => {
				console.log("Error occurred when trying to find KOL info, ", error);
				resolve(result);
			})
		}).catch(error => {
			console.log("Error occurred when trying to find KOL Ids in group key " + group_key + ": ", error);
			resolve(result);
		})
	})
}

function createJsonKolAdvocatorData(data, group_key) {
	return new Promise((resolve, reject) =>{
		// console.log("Data to be convert to JSON:", data);
		if (group_key) {
			// TODO
			// find KOL info base on group_key
		}

		var result = {
			response : {
				flagStatusData : true,
				resultData : []
			}
		}

		data.forEach(insight => {
			let info = {
				kol_fid: 		null,
				advocator_fid: 	insight._id.adv_id,
				post_id: 		insight._id.post_id,
				weight: 			insight.value.weight
			}
			result.response.resultData.push(info);
		})

		findUserIdOfGroupUser(group_key).then(foundIds => {
			getPostIdOfUserIds(foundIds, true).then(foundPostInfo => {
				result.response.resultData.forEach(advData => {
					foundPostInfo.forEach(foundInfo => {
						if (foundInfo.fid === advData.post_id) {
							advData.kol_fid = foundInfo.from_user
						}
					})
				});
				console.log("Updated advocator information successfully!");
				resolve(result);		
			}).catch(error => {
				console.log("Error occurred when trying to find posts info, ", error);
				resolve(result);
			})
		}).catch(error => {
			console.log("Error occurred when trying to find users Ids in group key " + group_key + ": ", error);
			resolve(result);
		})
	})
}

function findUserNames(user_ids) {
	return new Promise((resolve, reject) => {
		var result = [];
		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);	
			console.log("Connected correctly to server");
			db.collection('users').find({fid : {$in : user_ids}}, {_id :0, fid: 1, name :1, followers :1, nid: 1}).toArray(function(err, foundUserInfo) {
				// console.log("Found users", foundUserInfo);
				db.close();
				resolve(foundUserInfo);
			})
		})
	})
}

function findUserIdOfGroupUser(group_key) {
	return new Promise((resolve, reject) => { 
		MongoClient.connect(mongoConnectionString, function(err, db) {
			db.collection('groups').findOne({group_key : group_key}, {users : 1, _id : 0}, function(err, foundUsers) {
				if (err != null) console.log(err);
				// console.log("Get total " + foundUsers.users.length + " users of group key '" + group_key +  "' successfully!");
				db.close();
				resolve(foundUsers.users);
			})
		})
	})
}

// Tinh af, paid, earn cho cac bai post cua cac user trong group_key
function calculateInsightsPostsOfGroupUser(group_key, date_from, date_to) {
	return new Promise (function (resolve, reject) {
		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);
			console.log("Connected correctly to server");

			let mapReducePromises = [];

			var post_map = function(){
				var key = this.fid;
				var value = {
					total_paid_like : 0,
					total_paid_share : 0,
					total_paid_comment : 0,
					total_earn_like : 0,
					total_earn_share : 0,
					total_earn_comment : 0,
					post_content: this.message,
					published_date: this.created_date,
					crawled_date: this.updatedAt,
					user_id: this.from_user,
					post_id: this.fid,
					post_type: this.post_type,
					post_title: this.post_title != undefined ? this.post_title : null,
					post_thumbnail: this.post_thumbnail != undefined ? this.post_thumbnail : null
				}
				var size = this.interactions_count.length;
				if (size > 0) {
					var lastest_interaction = this.interactions_count[size - 1];
					value.total_paid_like = parseInt(lastest_interaction.count.likes_count),
					value.total_paid_share = parseInt(lastest_interaction.count.shares_count),
					value.total_paid_comment = parseInt(lastest_interaction.count.comments_count)
				}
				emit(key, value)
			}

			var share_map = function() {
				var key = this.parent_id;
				var value = {
					total_paid_like : 0,
					total_paid_share : 0,
					total_paid_comment : 0,
					total_earn_like : 0,
					total_earn_share : 0,
					total_earn_comment : 0,
					post_content: null,
					published_date: null,
					crawled_date: null,
					user_id: null,
					post_id: null,
					post_type: null,
					post_title: null,
					post_thumbnail: null
				}
				var size = this.interactions_count.length;
				if (size > 0) {
					var lastest_interaction = this.interactions_count[size - 1];
					value.total_earn_like = parseInt(lastest_interaction.count.likes_count),
					value.total_earn_share = parseInt(lastest_interaction.count.shares_count),
					value.total_earn_comment = parseInt(lastest_interaction.count.comments_count)
				}
				emit(key, value)
			}

			var reduce = function(key, values){
				var value = {
					af_point : 0,
					af_like_point: 0,
					af_share_point: 0,
					af_comment_point: 0,
					paid_value : 0,
					total_paid_like : 0,
					total_paid_share : 0,
					total_paid_comment : 0,
					earning_value : 0,
					total_earn_like : 0,
					total_earn_share: 0,
					total_earn_comment : 0,
					post_content: null,
					published_date: null,
					crawled_date: null,
					user_id: null,
					post_id: null,
					post_type: null,
					post_title: null,
					post_thumbnail: null
				}

				values.forEach(function(v) {
					value.total_paid_like += v.total_paid_like,
					value.total_paid_share += v.total_paid_share,
					value.total_paid_comment += v.total_paid_comment,
					value.total_earn_like += v.total_earn_like,
					value.total_earn_share += v.total_earn_share,
					value.total_earn_comment += v.total_earn_comment

					if (v.post_content != null) value.post_content = v.post_content;
					if (v.published_date !=null) value.published_date = v.published_date;
					if (v.crawled_date !=null) value.crawled_date = v.crawled_date;
					if (v.user_id !=null) value.user_id = v.user_id;
					if (v.post_id !=null) value.post_id = v.post_id;
					if (v.post_type !=null) value.post_type = v.post_type;
					if (v.post_title !=null) value.post_title = v.post_title;
					if (v.post_thumbnail !=null) value.post_thumbnail = v.post_thumbnail;
				})
				return value
			}

			var finalize = function(key, value){
		    	var point = { like : 1, comment : 3, share : 10	}
		        value.af_like_point = Math.round(((value.total_paid_like * point.like)*(value.total_paid_like * point.like) / 
		            (value.total_paid_like + value.total_paid_share + value.total_paid_comment))*100/100);
		        value.af_share_point = Math.round(((value.total_paid_share * point.share)*(value.total_paid_share * point.share) / 
		            (value.total_paid_like + value.total_paid_share + value.total_paid_comment))*100)/100;
		        value.af_comment_point = Math.round(((value.total_paid_comment * point.comment)*(value.total_paid_comment * point.comment) / 
		            (value.total_paid_like + value.total_paid_share + value.total_paid_comment))*100)/100;
		        value.af_point = value.af_like_point + value.af_share_point + value.af_comment_point;
		        value.paid_value = value.total_paid_like + value.total_paid_share + value.total_paid_comment;
		        value.earning_value = value.total_earn_like + value.total_earn_share + value.total_earn_comment;
		        return value
		    }

			let group_users = [];
			let user_post_ids = [];

			var out_collection = "posts_insights_" + group_key + "_" + date_from + "_" + date_to;

			db.collection('groups').findOne({group_key : group_key}, {users : 1, _id : 0}, function(err, foundUsers) {
				if (err != null) console.log(err);
				console.log("Get list of " + foundUsers.users.length + " users of group key '" + group_key +  "' successfully!");
				group_users = foundUsers.users;

				db.collection('posts').find({from_user : {$in : group_users}, parent_id: null}, {fid: 1, _id: 0}).toArray(function(err, foundPostIds) {
					if (err != null) console.log(err);
					console.log("Get list of " + foundPostIds.length + " post IDs of users of group key '" + group_key +  "' successfully!");

					foundPostIds.forEach(function(post) {
						user_post_ids.push(post.fid);
					});

					var post_options = {
						query: {
							created_date : {
								$gte : new Date(date_from),
								$lt : new Date(date_to)
							},
							parent_id : null,
							fid : { $in : user_post_ids }
						},
						out : {
							reduce : out_collection,
						},
						finalize: finalize,
						verbose : false
					}

					var share_option = {
						query: {
							parent_id : { $in : user_post_ids }
						},
						out : {
							reduce : out_collection
						},
						finalize: finalize,
						verbose : false
					}

					mapReducePromises.push(new Promise((resolve_lv1, reject_lv1) => {
						db.collection('posts').mapReduce(
							post_map,
							reduce,
							post_options,
							function (error, results, stats) {
								if (error != null) console.log(error);
								console.log("Map-Reduce for post successfully!");
								resolve_lv1(true);
							}
						)
					}))

					mapReducePromises.push(new Promise((resolve_lv1, reject_lv1) => {
						db.collection('posts').mapReduce(
							share_map,
							reduce,
							share_option,
							function (error, results, stats) {
								if (error != null) console.log(error);
								console.log("Map-Reduce for share posts successfully!");
								resolve_lv1(true);
							}
						)
					}))

					Promise.all(mapReducePromises).then(result => {
						resolve(true);
					})
				});
			});
		})
	})
}

// Tinh af, paid, earn cho cac user cua group_key
function calculateInsightsKolsOfGroupUser(group_key, date_from, date_to) {
	return new Promise (function (resolve, reject) {
		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);
			console.log("Connected correctly to server");

			var out_collection = "kols_insights_" + group_key + "_" + date_from + "_" + date_to;
			var input_collection = "posts_insights_" + group_key + "_" + date_from + "_" + date_to;

			var map = function(){
				var key = {
					fid : this.value.user_id
				}
				var value = {
					kol_like_point : this.value.af_like_point,
					kol_share_point : this.value.af_share_point,
					kol_comment_point : this.value.af_comment_point,
					kol_like_paid_value : this.value.total_paid_like,
					kol_share_paid_value : this.value.total_paid_share,
					kol_comment_paid_value : this.value.total_paid_comment,
					kol_like_earning_value : this.value.total_earn_like,
					kol_share_earning_value : this.value.total_earn_share,
					kol_comment_earning_value : this.value.total_earn_comment
				}
				emit(key, value)
			}

			var reduce = function(key, values){
				var value = {
					kol_af_point : 0,
					kol_like_point: 0,
					kol_share_point: 0,
					kol_comment_point: 0,
					kol_af_paid_value : 0,
					kol_like_paid_value : 0,
					kol_share_paid_value : 0,
					kol_comment_paid_value : 0,
					kol_af_earning_value : 0,
					kol_like_earning_value : 0,
					kol_share_earning_value: 0,
					kol_comment_earning_value : 0
				}

				values.forEach(function(v) {
					value.kol_like_point += v.kol_like_point,
					value.kol_share_point += v.kol_share_point,
					value.kol_comment_point += v.kol_comment_point,
					value.kol_like_paid_value += v.kol_like_paid_value,
					value.kol_share_paid_value += v.kol_share_paid_value,
					value.kol_comment_paid_value += v.kol_comment_paid_value,
					value.kol_like_earning_value += v.kol_like_earning_value,
					value.kol_share_earning_value += v.kol_share_earning_value,
					value.kol_comment_earning_value += v.kol_comment_earning_value
				})
				return value
			}

			var finalize = function(key, value){
				value.kol_af_point = value.kol_like_point + value.kol_comment_point + value.kol_share_point;
				value.kol_af_paid_value = value.kol_like_paid_value + value.kol_share_paid_value + value.kol_comment_paid_value;
				value.kol_af_earning_value = value.kol_like_earning_value + value.kol_share_earning_value + value.kol_comment_earning_value;
				return value
			}

			var options = {
				out : {
					reduce : out_collection,
				},
				finalize: finalize,
				verbose : false
			}

			db.collection(input_collection).count(function(err, total) {
				if (err != null) console.log(err);
				if (total === 0) {
					calculateInsightsPostsOfGroupUser(group_key, date_from, date_to).then(result => {
						db.collection(input_collection).mapReduce(
							map,
							reduce,
							options,
							function (error, results, stats) {
								if (error != null) console.log(error);
								console.log("Map-Reduce for post successfully!");
								resolve(true);
							}
						)
					}).catch(error => {
						console.log("Error when trying to calculate data for input collection! ", error);
					})
				} else {
					db.collection(input_collection).mapReduce(
						map,
						reduce,
						options,
						function (error, results, stats) {
							if (error != null) console.log(error);
							console.log("Map-Reduce for post successfully!");
							resolve(true);
						}
					)
				} 
			})
		})
	})
}

// Tinh thong tin cac advocator co tuong tac voi cac bai post cua user trong group_key
function calculateInsightsAdvocatorsOfGroupUser(group_key, date_from, date_to) {
	return new Promise (function (resolve, reject) {
		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);
			console.log("Connected correctly to server");
			let mapReducePromises = []

			var out_collection = "kols_insights_avocators_" + group_key + "_" + date_from + "_" + date_to;		

			var map = function(){
				var key = {
					post_id : this.parent_id,
					adv_id : this.from_user	
				}
				var value = {
					likes_count : 0,
					shares_count : 0,
					comments_count : 0
				}
				var size = this.interactions_count.length;
				if (size > 0) {
					var lastest_interaction = this.interactions_count[size - 1];
					value.likes_count = parseInt(lastest_interaction.count.likes_count),
					value.shares_count = parseInt(lastest_interaction.count.shares_count),
					value.comments_count = parseInt(lastest_interaction.count.comments_count)
				}
				emit(key, value)
			}

			var reduce = function(key, values){
				var value = {
					likes_count : 0,
					shares_count: 0,
					comments_count: 0,
					weight : 0
				}

				values.forEach(function(v) {
					value.likes_count += v.likes_count,
					value.shares_count += v.shares_count,
					value.comments_count += v.comments_count
				})
				return value
			}

			var finalize = function(key, value){
				value.weight = 1 + value.likes_count + value.shares_count + value.comments_count;
				return value
			}

			findUserIdOfGroupUser(group_key).then(user_ids => {
				getPostIdOfUserIds(user_ids).then(user_post_ids => {
					var options = {
						query: {
							parent_id : { $in : user_post_ids }
						},
						out : {
							reduce : out_collection
						},
						finalize: finalize,
						verbose : false
					}

					db.collection('posts').mapReduce(
						map,
						reduce,
						options,
						function (error, results, stats) {
							if (error != null) console.log(error);
							console.log("Map-Reduce for shares successfully!");
							resolve(true);
						}
					)
				}).catch(error => {
					console.log("Error when finding post ids of users. ", error);
				})
			}).catch(error => {
				console.log("Error when finding user ids of group. ", error);
			})
			
		})
	})
}

function getPostIdOfUserIds(user_ids, from_user = false) {
	return new Promise((resolve, reject) => { 
		MongoClient.connect(mongoConnectionString, function(err, db) {
			assert.equal(null, err);
			let user_post_ids = [];
			console.log("Connected correctly to server");
			let options = {fid: 1, _id: 0};
			if (from_user == true) {
				options.from_user = 1;
			}
			db.collection('posts').find({from_user : {$in : user_ids}, parent_id : null}, options).toArray(function(err, foundPostIds) {
				if (err != null) console.log(err);
				console.log("Get list of " + foundPostIds.length + " post IDs of input users successfully!");
				if (from_user == true) {
					resolve(foundPostIds);
				}
				foundPostIds.forEach(function(post) {
					user_post_ids.push(post.fid);
				});
				db.close();
				resolve(user_post_ids);
			})
		});
	})
}