'use strict';

// post-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const interactionSchema = new Schema({ 
	date : Date, 
	count : {
		comments_count : String, 
		shares_count : String, 
		likes_count : String 
	}}, 
	{ _id: false }
);

const postSchema = new Schema({
	fid: 			{ type: String, required: true, unique: true },
	message: 		{ type: String },
	from_user: 		{ type: String },
	parent_id: 		{ type: String },
	created_date: 	{ type: Date },
	createdAt: 		{ type: Date, 'default': Date.now },
	updatedAt: 		{ type: Date, 'default': Date.now },
	sentiment:  		{ type: String },
	entities: 		{ type: Array, 'default': [] }
});

const postModel = mongoose.model('post', postSchema);

module.exports = postModel;