'use strict';
const post = require('./post');
const keyword = require('./keyword');
const authentication = require('./authentication');
const mongoose = require('mongoose');

module.exports = function() {
  const app = this;

  mongoose.connect(app.get('mongodb'));
  mongoose.Promise = global.Promise;

  app.configure(authentication);
  app.configure(post);
  app.configure(keyword);
};
