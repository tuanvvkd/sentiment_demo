'use strict';

const service = require('feathers-mongoose');
const keyword = require('./keyword-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: keyword,
    paginate: {
      default: 1000,
      max: 1000
    }
  };

  // Initialize our service with any options it requires
  app.use('/keywords', service(options));

  // Get our initialize service to that we can bind hooks
  const keywordService = app.service('/keywords');

  // Set up our before hooks
  keywordService.before(hooks.before);

  // Set up our after hooks
  keywordService.after(hooks.after);
};
