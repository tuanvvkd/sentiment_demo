'use strict';

// post-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const keywordSchema = new Schema({
	key: 		{ type: String, required: true, unique: true},
	group: 		{ type: String }
});

const keywordModel = mongoose.model('keyword', keywordSchema);

module.exports = keywordModel;