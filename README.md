# AdAsia

[![Build Status](https://travis-ci.org/feathersjs/feathers-chat.png?branch=master)](https://travis-ci.org/feathersjs/feathers-chat)

> A tool for automation crawling data from Facebook.
> API for returning data to MySQL

## This project uses:

1. [Feathers](http://feathersjs.com). An open source web  framework for building modern real-time applications.
2. [Facebook  Graph](https://developers.facebook.com/docs/graph-api), the provided API for reading and writing data to Facebook social graph.
3. [Selenium](http://www.seleniumhq.org), a free tool for web automation.

## License

Copyright (c) 2017

Licensed under the [MIT license](LICENSE).