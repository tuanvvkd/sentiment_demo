// calculate_share_of_voice_for_keyword
function (keywords, date_from, date_to) {
    var result = [];
    keywords.forEach(function(keyword){
        var total_post_mention = db.posts.find({message : {$regex: keyword}, created_date: {$gte: ISODate(date_from + '00:00:00'), $lt: ISODate(date_to + '00:00:00') } }).count();
        var total_post = db.posts.find({created_date: {$gte: ISODate(date_from + '00:00:00'), $lt: ISODate(date_to + '00:00:00') } }).count();
        var total_comment_mention = db.comments.find({message : {$regex: keyword}, created_date: {$gte: ISODate(date_from + '00:00:00'), $lt: ISODate(date_to + '00:00:00') } }).count();
        var total_comment = db.comments.find({created_date: {$gte: ISODate(date_from + '00:00:00'), $lt: ISODate(date_to + '00:00:00') } }).count();
        
        var total_mention = total_post_mention + total_comment_mention;
        var total = total_post + total_comment;
        var share_of_voice = (total_mention*100)/total;
        var item = {
            keyword : {
                total_mention : total_mention,
                total : total,
                share_of_voice : share_of_voice,
                date_from : date_from,
                date_to : date_to
            }
        }
        result.push(item);
    });
    return result;
}
var keywords = ['sony', 'samsung'];
var result = cal_share_of_voice(keywords, '2017-01-01', '2017-08-01');
print(result);



}