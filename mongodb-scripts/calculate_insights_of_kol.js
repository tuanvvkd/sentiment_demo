function cal_insights_kol_of_user_group(group_key, dateFrom, dateTo) {
    var out_collection = "kols_insight_" + group_key + "_" + dateFrom + "_" + dateTo;
    db.getCollection(out_collection).drop();
    
    var input_collection = "posts_insight_" + group_key + "_" + dateFrom + "_" + dateTo;

    var is_created = db.getCollection(input_collection).count();
    if (is_created == 0) {
        db.loadServerScripts();
        cal_insights_posts_of_user_group(group_key, dateFrom, dateTo);            
    }

    var reduce = function(key, values){
        var value = {
            kol_af_point : 0,
            kol_like_point: 0,
            kol_share_point: 0,
            kol_comment_point: 0,
            kol_af_paid_value : 0,
            kol_like_paid_value : 0,
            kol_share_paid_value : 0,
            kol_comment_paid_value : 0,
            kol_af_earning_value : 0,
            kol_like_earning_value : 0,
            kol_share_earning_value: 0,
            kol_comment_earning_value : 0
        }
        
        values.forEach(function(v) {
            value.kol_like_point += v.kol_like_point,
            value.kol_share_point += v.kol_share_point,
            value.kol_comment_point += v.kol_comment_point,
            value.kol_like_paid_value += v.kol_like_paid_value,
            value.kol_share_paid_value += v.kol_share_paid_value,
            value.kol_comment_paid_value += v.kol_comment_paid_value,
            value.kol_like_earning_value += v.kol_like_earning_value,
            value.kol_share_earning_value += v.kol_share_earning_value,
            value.kol_comment_earning_value += v.kol_comment_earning_value
        })
        return value
    }

    var finalizeFuncion = function(key, value){
        value.kol_af_point = value.kol_like_point + value.kol_comment_point + value.kol_share_point;
        value.kol_af_paid_value = value.kol_like_paid_value + value.kol_share_paid_value + value.kol_comment_paid_value;
        value.kol_af_earning_value = value.kol_like_earning_value + value.kol_share_earning_value + value.kol_comment_earning_value;
        return value
    }
   
    var map = function(){
        var key = {
            fid : this._id
        }
        var value = {
            kol_like_point : this.value.af_like_point,
            kol_share_point : this.value.af_share_point,
            kol_comment_point : this.value.af_comment_point,
            kol_like_paid_value : this.value.total_paid_like,
            kol_share_paid_value : this.value.total_paid_share,
            kol_comment_paid_value : this.value.total_paid_comment,
            kol_like_earning_value : this.value.total_earn_like,
            kol_share_earning_value : this.value.total_earn_share,
            kol_comment_earning_value : this.value.total_earn_comment
        }
        emit(key, value)
    }

    db.getCollection(input_collection).mapReduce(
        map,
        reduce,
        {
            out : {
                reduce : out_collection,
            },
            finalize: finalizeFuncion
        }
    )
}

// cal_insights_kol_of_user_group("kol", "2016-01-01", "2017-09-01");