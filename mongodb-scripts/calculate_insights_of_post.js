function cal_insights_posts_of_user_group(group_key, dateFrom, dateTo) {
	var out_collection = "posts_insight_" + group_key + "_" + dateFrom + "_" + dateTo;
    db.getCollection(out_collection).drop();

    var group_data = db.groups.find({group_key : group_key}).toArray();
    var user_ids = group_data[0].users;

    var posts = db.posts.find({from_user : {$in : user_ids}}, {_id: 0, fid: 1}).toArray();
    var post_fids = [];
    
    posts.forEach(function(post) {
        post_fids.push(post.fid);
    })
   
    var map_post = function(){
        key = this.fid;
        var value = {
        	total_paid_like : 0,
        	total_paid_share : 0,
        	total_paid_comment : 0,
        	total_earn_like : 0,
        	total_earn_share : 0,
        	total_earn_comment : 0,
        	post_content: this.message,
        	published_date: this.created_date,
        	crawled_date: this.updatedAt,
        	user_id: this.from_user,
        	post_id: this.fid,
        	post_type: this.post_type,
        	post_title: this.post_title != undefined ? this.post_title : null,
        	post_thumbnail: this.post_thumbnail != undefined ? this.post_thumbnail : null
        }
        var size = this.interactions_count.length;
        if (size > 0) {
        	var lastest_interaction = this.interactions_count[size - 1];
        	value.total_paid_like = parseInt(lastest_interaction.count.likes_count),
        	value.total_paid_share = parseInt(lastest_interaction.count.shares_count),
        	value.total_paid_comment = parseInt(lastest_interaction.count.comments_count)
        }
        emit(key, value)
    }

    var map_share = function() {
    	key = this.parent_id;
        var value = {
        	total_paid_like : 0,
        	total_paid_share : 0,
        	total_paid_comment : 0,
        	total_earn_like : 0,
        	total_earn_share : 0,
        	total_earn_comment : 0,
        	post_content: null,
        	published_date: null,
        	crawled_date: null,
        	user_id: null,
        	post_id: null,
        	post_type: null,
        	post_title: null,
        	post_thumbnail: null
        }
        var size = this.interactions_count.length;
        if (size > 0) {
        	var lastest_interaction = this.interactions_count[size - 1];
        	value.total_earn_like = parseInt(lastest_interaction.count.likes_count),
        	value.total_earn_share = parseInt(lastest_interaction.count.shares_count),
        	value.total_earn_comment = parseInt(lastest_interaction.count.comments_count)
        }
        emit(key, value)
    }

    var reduce = function(key, values){
        var value = {
            af_point : 0,
            af_like_point: 0,
            af_share_point: 0,
            af_comment_point: 0,
            paid_value : 0,
            total_paid_like : 0,
            total_paid_share : 0,
            total_paid_comment : 0,
            earning_value : 0,
            total_earn_like : 0,
            total_earn_share: 0,
            total_earn_comment : 0,
            post_content: null,
			published_date: null,
			crawled_date: null,
			user_id: null,
			post_id: null,
			post_type: null,
			post_title: null,
			post_thumbnail: null
        }
        
        values.forEach(function(v) {
            value.total_paid_like += v.total_paid_like,
            value.total_paid_share += v.total_paid_share,
            value.total_paid_comment += v.total_paid_comment,
            value.total_earn_like += v.total_earn_like,
            value.total_earn_share += v.total_earn_share,
            value.total_earn_comment += v.total_earn_comment

            if (v.post_content != null) value.post_content = v.post_content;
			if (v.published_date !=null) value.published_date = v.published_date;
			if (v.crawled_date !=null) value.crawled_date = v.crawled_date;
			if (v.user_id !=null) value.user_id = v.user_id;
			if (v.post_id !=null) value.post_id = v.post_id;
			if (v.post_type !=null) value.post_type = v.post_type;
			if (v.post_title !=null) value.post_title = v.post_title;
			if (v.post_thumbnail !=null) value.post_thumbnail = v.post_thumbnail;
        })
        return value
    }

    var finalizeFuncion = function(key, value){
    	var point = { like : 1, comment : 3, share : 10	}
        value.af_like_point = Math.round(((value.total_paid_like * point.like)*(value.total_paid_like * point.like) / 
            (value.total_paid_like + value.total_paid_share + value.total_paid_comment))*100/100);
        value.af_share_point = Math.round(((value.total_paid_share * point.share)*(value.total_paid_share * point.share) / 
            (value.total_paid_like + value.total_paid_share + value.total_paid_comment))*100)/100;
        value.af_comment_point = Math.round(((value.total_paid_comment * point.comment)*(value.total_paid_comment * point.comment) / 
            (value.total_paid_like + value.total_paid_share + value.total_paid_comment))*100)/100;
        value.af_point = value.af_like_point + value.af_share_point + value.af_comment_point;
        value.paid_value = value.total_paid_like + value.total_paid_share + value.total_paid_comment;
        value.earning_value = value.total_earn_like + value.total_earn_share + value.total_earn_comment;
        return value
    }

    db.posts.mapReduce(
        map_post,
        reduce,
        {
            query: {
            	created_date : {
                    $gte : ISODate(dateFrom),
                    $lt : ISODate(dateTo)
                },
                parent_id : null,
                fid : { $in : post_fids }
            },
            out : {
            	reduce : out_collection,
            },
            finalize: finalizeFuncion
        }
    )

    db.posts.mapReduce(
        map_share,
        reduce,
        {
            query: {
                parent_id : { $in : post_fids }
            },
            out : {
        		reduce : out_collection
        	},
            finalize: finalizeFuncion
        }
    )
}

cal_insights_posts_of_user_group("kol", "2016-01-01", "2017-09-01");