'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('lstpostid service', function() {
  it('registered the lstpostids service', () => {
    assert.ok(app.service('lstpostids'));
  });
});
