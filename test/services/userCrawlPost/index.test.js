'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('userCrawlPost service', function() {
  it('registered the userCrawlPosts service', () => {
    assert.ok(app.service('userCrawlPosts'));
  });
});
