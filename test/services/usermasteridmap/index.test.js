'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('usermasteridmap service', function() {
  it('registered the usermasteridmaps service', () => {
    assert.ok(app.service('usermasteridmaps'));
  });
});
