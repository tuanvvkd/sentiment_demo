'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('keyword service', function() {
  it('registered the keywords service', () => {
    assert.ok(app.service('keywords'));
  });
});
